package com.currencies.data.repository

import com.currencies.data.remote.store.LocalCurrencyStore
import com.currencies.domain.repository.LocalCurrencyRepository

class LocalCurrencyRepositoryImpl(
    private val localCurrencyStore: LocalCurrencyStore
) : LocalCurrencyRepository {

    override val localCurrencyName get() = localCurrencyStore.localCurrency
}
