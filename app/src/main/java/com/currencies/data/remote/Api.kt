package com.currencies.data.remote

import com.currencies.data.remote.entity.GetConvertResultResponse
import com.currencies.data.remote.entity.GetLatestCurrenciesResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("/latest?access_key=4242b024a05b111a00ffc6653a5f4314")
    suspend fun getLatestCurrencies(): GetLatestCurrenciesResponse

    @GET("/convert?access_key=4242b024a05b111a00ffc6653a5f4314")
    suspend fun getConvertAmount(
        @Query("from") from: String,
        @Query("to") to: String,
        @Query("amount") amount: Double
    ): GetConvertResultResponse
}
