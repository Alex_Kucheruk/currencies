package com.currencies.presentation.main.currencies.list.common

import androidx.compose.runtime.Immutable
import com.currencies.core.Error
import com.currencies.domain.entity.ICurrency

@Immutable
data class CurrenciesUIState<T : ICurrency>(
    val currencies: List<T> = listOf()
)

sealed interface CurrenciesScreenUIIntent {
    data class ShowConvertCurrency(val fromCurrency: String, val toCurrency: String):
        CurrenciesScreenUIIntent

    data class ShowError(val error: Error): CurrenciesScreenUIIntent
}

sealed interface CurrenciesScreenUIEvent {
    data class AddCurrency(val currency: String): CurrenciesScreenUIEvent
    data class RemoveCurrency(val currency: String): CurrenciesScreenUIEvent

    data class OnClickCurrency(val currency: String): CurrenciesScreenUIEvent
}

