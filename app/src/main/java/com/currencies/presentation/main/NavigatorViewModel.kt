package com.currencies.presentation.main

import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow

class NavigatorViewModel: ViewModel() {

    private val _destinationSharedFlow = MutableSharedFlow<NavDirections>(
        extraBufferCapacity = 1,
        onBufferOverflow = BufferOverflow.DROP_OLDEST,
    )
    val destinationSharedFlow = _destinationSharedFlow.asSharedFlow()

    fun navigateTo(navDirection: NavDirections) {
        _destinationSharedFlow.tryEmit(navDirection)
    }

}