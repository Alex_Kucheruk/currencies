package com.currencies.presentation.main.currencies

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.currencies.R
import com.currencies.databinding.FragmentCurrenciesNavBinding
import com.currencies.utils.view_binding.viewBinding

class CurrenciesFragment : Fragment(R.layout.fragment_currencies_nav) {

    private val binding by viewBinding(FragmentCurrenciesNavBinding::bind)

    private lateinit var navController: NavController

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navHostFragment = childFragmentManager.findFragmentById(R.id.nav_host_currencies) as NavHostFragment
        navController = navHostFragment.navController
        binding.bottomNavView.setupWithNavController(navController)
    }

}