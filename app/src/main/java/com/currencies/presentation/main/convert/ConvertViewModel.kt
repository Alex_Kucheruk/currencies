package com.currencies.presentation.main.convert

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.currencies.core.emitWithScope
import com.currencies.domain.usecase.ConvertUseCase
import com.currencies.utils.childScope
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlin.random.Random

class ConvertViewModel(
    private val fromCurrencyName: String,
    private val toCurrencyName: String,
    private val convertUseCase: ConvertUseCase
) : ViewModel() {

    private val searchScope by childScope()

    private val mutableUiState = MutableStateFlow(
        ConvertUIState(
            fromCurrencyName = fromCurrencyName,
            toCurrencyName = toCurrencyName
        )
    )
    val uiState = mutableUiState.asStateFlow()

    private val mutableUiIntent = MutableSharedFlow<ConvertScreenUIIntent>()
    val uiIntent = mutableUiIntent.asSharedFlow()

    fun handleEvent(event: ConvertScreenUIEvent) {
        when (event) {
            is ConvertScreenUIEvent.ConvertAmount -> convert()
        }
    }

    fun updateInputAmount(inputAmount: String) {
        mutableUiState.update { it.copy(inputAmount = inputAmount) }
    }

    private fun convert() = with(searchScope) {
        coroutineContext.cancelChildren()
        launch {
            convertUseCase.launch(
                fromCurrencyName,
                toCurrencyName,
                uiState.value.inputAmount.toDoubleOrNull() ?: 0.0
            )
                .fold(
                    onSuccess = {
                        mutableUiState.update {
                            it.copy(
                                convertResult = Random.nextDouble().toString().take(5),
                                inputAmount = ""
                            )
                        }
                    },
                    onFailure = {
                        mutableUiIntent.emitWithScope(viewModelScope, ConvertScreenUIIntent.ShowError(it))
                        mutableUiState.update { state -> state.copy(inputAmount = "") }
                    }
                )
        }
    }
}
