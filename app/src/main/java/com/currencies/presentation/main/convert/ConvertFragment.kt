package com.currencies.presentation.main.convert

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.currencies.presentation.theme.ColorPrimary
import com.currencies.utils.showError
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ConvertFragment : Fragment() {

    private val args by navArgs<ConvertFragmentArgs>()

    private val viewModel by viewModel<ConvertViewModel> {
        parametersOf(args.currencyFrom, args.currencyTo)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                ConvertCurrencyScreen()
            }
        }
    }

    @Composable
    fun ConvertCurrencyScreen() {
        val uiState by viewModel.uiState.collectAsStateWithLifecycle()
        Scaffold { paddingValues ->
            Column(modifier = Modifier.padding(paddingValues)) {
                Row(modifier = Modifier.padding(horizontal = 24.dp)) {
                    Text(
                        text = uiState.fromCurrencyName,
                        modifier = Modifier
                            .weight(weight = 1F),
                        fontSize = 20.sp,
                        color = ColorPrimary
                    )
                    BasicTextField(
                        modifier = Modifier
                            .background(Color(0xFFBDBDBD))
                            .width(80.dp)
                            .padding(vertical = 4.dp, horizontal = 4.dp),
                        singleLine = true,
                        textStyle = TextStyle.Default.copy(
                            fontSize = 20.sp,
                        ),
                        value = uiState.inputAmount,
                        onValueChange = viewModel::updateInputAmount,
                        keyboardOptions = KeyboardOptions(
                            imeAction = ImeAction.Go,
                            keyboardType = KeyboardType.Number
                        ),
                        keyboardActions = KeyboardActions(
                            onGo = {
                                viewModel.handleEvent(ConvertScreenUIEvent.ConvertAmount)
                            }
                        )
                    )
                }

                Spacer(modifier = Modifier.height(16.dp))
                Row(modifier = Modifier.padding(horizontal = 24.dp)) {
                    Text(
                        text = uiState.toCurrencyName,
                        modifier = Modifier.weight(weight = 1F),
                        fontSize = 20.sp,
                        color = ColorPrimary
                    )
                    Text(
                        text = uiState.convertResult,
                        fontSize = 20.sp,
                        modifier = Modifier
                            .background(color = Color(0xFFE0E0E0))
                            .defaultMinSize(minWidth = 80.dp)
                            .padding(vertical = 4.dp, horizontal = 4.dp)
                    )
                }
            }
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //-
        lifecycleScope.launch {
            viewModel.uiIntent.collect {
                when (it) {
                    is ConvertScreenUIIntent.ShowError -> showError(it.error)
                }
            }
        }
    }
}
