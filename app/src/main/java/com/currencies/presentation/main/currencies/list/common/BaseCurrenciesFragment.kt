package com.currencies.presentation.main.currencies.list.common

import android.os.Bundle
import android.view.View
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.currencies.R
import com.currencies.domain.entity.ICurrency
import com.currencies.presentation.main.NavigatorViewModel
import com.currencies.presentation.main.currencies.CurrenciesFragmentDirections
import com.currencies.presentation.theme.ColorPrimary
import com.currencies.utils.showError
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

abstract class BaseCurrenciesFragment<T: ICurrency>: Fragment() {

    abstract val viewModel: CurrenciesViewModel<T>
    private val navigationViewModel by sharedViewModel<NavigatorViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //-
        lifecycleScope.launch {
            viewModel.uiIntent.collect {
                when (it) {
                    is CurrenciesScreenUIIntent.ShowConvertCurrency -> {
                        val destination =
                            CurrenciesFragmentDirections.actionCurrenciesFragmentToConvertFragment(
                                it.fromCurrency,
                                it.toCurrency
                            )
                        navigationViewModel.navigateTo(destination)
                    }

                    is CurrenciesScreenUIIntent.ShowError -> showError(it.error)
                }
            }
        }
    }

    @Composable
    protected fun ShowNoResults() {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = stringResource(R.string.no_results),
                textAlign = TextAlign.Center,
                fontSize = 26.sp,
                color = ColorPrimary
            )
        }
    }
}