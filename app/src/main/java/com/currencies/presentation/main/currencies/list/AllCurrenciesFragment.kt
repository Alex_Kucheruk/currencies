package com.currencies.presentation.main.currencies.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.currencies.R
import com.currencies.domain.entity.AllCurrency
import com.currencies.presentation.main.currencies.list.common.BaseCurrenciesFragment
import com.currencies.presentation.main.currencies.list.common.CurrenciesScreenUIEvent
import com.currencies.presentation.main.currencies.list.common.CurrenciesViewModel
import com.currencies.presentation.main.currencies.list.common.Qualifier
import com.currencies.presentation.theme.ColorAccent
import com.currencies.presentation.theme.ColorPrimary
import org.koin.androidx.viewmodel.ext.android.viewModel

class AllCurrenciesFragment : BaseCurrenciesFragment<AllCurrency>() {

    override val viewModel by viewModel<CurrenciesViewModel<AllCurrency>>(Qualifier.AllCurrencies)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                AllCurrencyScreen()
            }
        }
    }

    @Composable
    private fun AllCurrencyScreen() {
        val uiState by viewModel.uiState.collectAsStateWithLifecycle()
        Scaffold { paddingValues ->
            // TODO handle progress (shimmer https://proandroiddev.com/shimmer-shadow-loading-effect-animation-with-jetpack-compose-f4b3de28dc2b)
            if (uiState.currencies.isEmpty()) {
                ShowNoResults()
            } else {
                LazyColumn(contentPadding = PaddingValues(top = paddingValues.calculateTopPadding())) {
                    itemsIndexed(
                        uiState.currencies,
                        key = { _, item -> item.currencyName }) { index, item ->
                        if (index == 0) {
                            Spacer(modifier = Modifier.height(12.dp))
                        }
                        CurrencyItem(
                            modifier = Modifier
                                .padding(horizontal = 16.dp)
                                .height(50.dp),
                            handleEvent = viewModel::handleEvent,
                            item = item
                        )
                        Spacer(modifier = Modifier.height(6.dp))
                    }
                }
            }
        }
    }

    @Composable
    private fun CurrencyItem(
        modifier: Modifier = Modifier,
        handleEvent: (CurrenciesScreenUIEvent) -> Unit,
        item: AllCurrency
    ) {
        Row(
            modifier = modifier.clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = null
            ) {
                handleEvent(CurrenciesScreenUIEvent.OnClickCurrency(item.currencyName))
            },
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Text(
                text = item.currencyName,
                modifier = Modifier.weight(weight = 1F),
                fontSize = 20.sp,
                color = ColorPrimary
            )
            Button(
                modifier = Modifier.width(80.dp),
                contentPadding = PaddingValues(6.dp, 2.dp),
                elevation = ButtonDefaults.elevatedButtonElevation(defaultElevation = 4.dp),
                shape = RoundedCornerShape(15),
                colors = ButtonDefaults.buttonColors(
                    containerColor = ColorAccent,
                    contentColor = Color.White
                ),
                onClick = {
                    if (item.isAdded) {
                        handleEvent(CurrenciesScreenUIEvent.RemoveCurrency(item.currencyName))
                    } else {
                        handleEvent(CurrenciesScreenUIEvent.AddCurrency(item.currencyName))
                    }
                }) {
                Text(
                    text = if (item.isAdded) {
                        stringResource(R.string.remove)
                    } else stringResource(R.string.add),
                    fontSize = 16.sp
                )
            }
        }
    }
}
