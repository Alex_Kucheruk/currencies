package com.currencies.presentation.main.currencies.list.common

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.currencies.core.emitWithScope
import com.currencies.domain.entity.ICurrency
import com.currencies.domain.usecase.GetLocalCurrencyUseCase
import com.currencies.domain.usecase.ICurrencyInteractor
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class CurrenciesViewModel<T : ICurrency>(
    private val currencyInteractor: ICurrencyInteractor<T>,
    private val localCurrencyUseCase: GetLocalCurrencyUseCase
) : ViewModel() {

    init {
        init()
        subscribe()
    }

    private val mutableUiState = MutableStateFlow(CurrenciesUIState<T>())
    val uiState = mutableUiState.asStateFlow()

    private val mutableUiIntent = MutableSharedFlow<CurrenciesScreenUIIntent>()
    val uiIntent = mutableUiIntent.asSharedFlow()

    private var localCurrencyName = ""

    private fun init() {
        viewModelScope.launch {
            localCurrencyUseCase.launch()
                .onSuccess { localCurrencyName = it }
        }
        viewModelScope.launch {
            currencyInteractor.init()
                .onFailure {
                    mutableUiIntent.emitWithScope(
                        viewModelScope,
                        CurrenciesScreenUIIntent.ShowError(it)
                    )
                }
        }
    }

    fun handleEvent(event: CurrenciesScreenUIEvent) {
        when (event) {
            is CurrenciesScreenUIEvent.AddCurrency -> addCurrency(event.currency)
            is CurrenciesScreenUIEvent.RemoveCurrency -> removeCurrency(event.currency)
            is CurrenciesScreenUIEvent.OnClickCurrency -> {
                mutableUiIntent.emitWithScope(
                    viewModelScope, CurrenciesScreenUIIntent.ShowConvertCurrency(
                        fromCurrency = localCurrencyName,
                        toCurrency = event.currency
                    )
                )
            }
        }
    }

    private fun subscribe() {
        viewModelScope.launch {
            currencyInteractor.subscribe().collect { result ->
                result.onSuccess {
                    mutableUiState.update { state ->
                        state.copy(currencies = it)
                    }
                }
                result.onFailure {
                    mutableUiIntent.emitWithScope(
                        viewModelScope,
                        CurrenciesScreenUIIntent.ShowError(it)
                    )
                }
            }
        }
    }

    private fun removeCurrency(currencyName: String) {
        viewModelScope.launch {
            currencyInteractor.removeCurrency(currencyName)
                .onFailure {
                    mutableUiIntent.emitWithScope(
                        viewModelScope,
                        CurrenciesScreenUIIntent.ShowError(it)
                    )
                }
        }
    }

    private fun addCurrency(currencyName: String) {
        viewModelScope.launch {
            currencyInteractor.addCurrency(currencyName)
                .onFailure {
                    mutableUiIntent.emitWithScope(
                        viewModelScope,
                        CurrenciesScreenUIIntent.ShowError(it)
                    )
                }
        }
    }

    /*
    fun search(query: String?) {
        viewModelScope.launch {
            currencyInteractor.searchCurrencies(query.orEmpty().trim())
                .onFailure {
                    mutableUiIntent.emitWithScope(
                        viewModelScope,
                        CurrenciesScreenUIIntent.ShowError(it)
                    )
                }
        }
    }
     */
}
