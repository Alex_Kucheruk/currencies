package com.currencies.presentation.main.currencies.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.animation.core.spring
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.AnchoredDraggableState
import androidx.compose.foundation.gestures.DraggableAnchors
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.anchoredDraggable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.currencies.R
import com.currencies.domain.entity.Currency
import com.currencies.presentation.main.currencies.list.common.BaseCurrenciesFragment
import com.currencies.presentation.main.currencies.list.common.CurrenciesScreenUIEvent
import com.currencies.presentation.main.currencies.list.common.CurrenciesViewModel
import com.currencies.presentation.main.currencies.list.common.Qualifier
import com.currencies.presentation.theme.ColorPrimary
import com.currencies.presentation.theme.ColorRed
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.math.roundToInt

class MyCurrenciesFragment : BaseCurrenciesFragment<Currency>() {

    override val viewModel by viewModel<CurrenciesViewModel<Currency>>(Qualifier.MyCurrencies)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                MyCurrencyScreen()
            }
        }
    }

    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    private fun MyCurrencyScreen() {
        val uiState by viewModel.uiState.collectAsStateWithLifecycle()
        Scaffold { paddingValues ->
            if (uiState.currencies.isEmpty()) {
                ShowNoResults()
            } else {
                LazyColumn(contentPadding = PaddingValues(top = paddingValues.calculateTopPadding())) {
                    itemsIndexed(
                        items = uiState.currencies,
                        key = { _, item -> item.currencyName }) { index, item ->
                        if (index == 0) {
                            Spacer(modifier = Modifier.height(12.dp))
                        }
                        val deleteComponentWidthSize = 130.dp
                        MAnchoredDraggableBox(
                            modifier = Modifier.height(50.dp)
                                .animateItemPlacement(),
                            firstContent = { modifier ->
                                MyCurrencyItem(
                                    modifier = modifier
                                        .padding(horizontal = 16.dp)
                                        .fillMaxHeight(),
                                    handleEvent = viewModel::handleEvent,
                                    item = item
                                )
                            },
                            secondContent = { modifier ->
                                DeleteItem(
                                    modifier = modifier
                                        .width(deleteComponentWidthSize)
                                        .fillMaxHeight(),
                                    handleEvent = viewModel::handleEvent,
                                    item = item
                                )
                            },
                            offsetSize = deleteComponentWidthSize
                        )
                        Spacer(modifier = Modifier.height(6.dp))
                    }
                }
            }
        }
    }

    @Composable
    private fun MyCurrencyItem(
        modifier: Modifier = Modifier,
        handleEvent: (CurrenciesScreenUIEvent) -> Unit,
        item: Currency
    ) {
        Row(
            modifier = modifier.clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = null
            ) {
                handleEvent(CurrenciesScreenUIEvent.OnClickCurrency(item.currencyName))
            },
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = item.currencyName,
                modifier = Modifier.weight(weight = 1F),
                fontSize = 20.sp,
                color = ColorPrimary
            )
        }
    }

    @Composable
    private fun DeleteItem(
        modifier: Modifier,
        handleEvent: (CurrenciesScreenUIEvent) -> Unit,
        item: Currency
    ) {
        Button(
            modifier = modifier,
            elevation = ButtonDefaults.elevatedButtonElevation(defaultElevation = 4.dp),
            shape = RoundedCornerShape(15),
            colors = ButtonDefaults.buttonColors(
                containerColor = ColorRed,
                contentColor = Color.White
            ),
            onClick = {
                handleEvent(CurrenciesScreenUIEvent.RemoveCurrency(item.currencyName))
            }) {
            Text(
                text = stringResource(R.string.remove),
                fontSize = 16.sp
            )
        }
    }

    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    private fun MAnchoredDraggableBox(
        modifier: Modifier,
        firstContent: @Composable (modifier: Modifier) -> Unit,
        secondContent: @Composable (modifier: Modifier) -> Unit,
        offsetSize: Dp
    ) {
        val density = LocalDensity.current
        val positionalThresholds: (totalDistance: Float) -> Float =
            { totalDistance -> totalDistance * 0.5f }
        val velocityThreshold: () -> Float = { with(density) { 100.dp.toPx() } }

        val state = remember {
            AnchoredDraggableState(
                initialValue = DragAnchors.Start,
                positionalThresholds,
                velocityThreshold,
                animationSpec = spring()
            ).apply {
                val newAnchors = with(density) {
                    DraggableAnchors {
                        DragAnchors.Start at 0.dp.toPx()
                        DragAnchors.End at -offsetSize.toPx()
                    }
                }
                updateAnchors(newAnchors)
            }
        }

        Box(
            modifier = modifier
        ) {
            firstContent(
                Modifier
                    .fillMaxWidth()
                    .offset {
                        IntOffset(
                            state
                                .requireOffset()
                                .roundToInt(), 0
                        )
                    }
                    .anchoredDraggable(state, Orientation.Horizontal)
            )
            secondContent(
                Modifier
                    .align(Alignment.CenterEnd)
                    .offset {
                        IntOffset(
                            (state.requireOffset() + offsetSize.toPx()).roundToInt(), 0
                        )
                    }
                    .anchoredDraggable(state, Orientation.Horizontal)
            )
        }
    }


    enum class DragAnchors {
        Start,
        End,
    }
}
