package com.currencies.presentation.main.convert

import androidx.compose.runtime.Immutable
import com.currencies.core.Error

@Immutable
data class ConvertUIState(
    val fromCurrencyName: String = "",
    val toCurrencyName: String = "",
    val convertResult: String = "",
    val inputAmount: String = ""
)

sealed interface ConvertScreenUIIntent {
    data class ShowError(val error: Error): ConvertScreenUIIntent
}

sealed interface ConvertScreenUIEvent {
    data object ConvertAmount: ConvertScreenUIEvent
}