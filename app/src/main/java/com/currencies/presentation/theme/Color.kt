package com.currencies.presentation.theme

import androidx.compose.ui.graphics.Color

val ColorPrimary = Color(0xFF04446B)
val ColorAccent = Color(0xFF81C4EE)
val ColorRed = Color.Red
val ColorBlueLight = Color(0xFFF5F8FF)
val ColorGray = Color(0xFF626C87)
val ColorGray70 = Color(0xB3626C87)