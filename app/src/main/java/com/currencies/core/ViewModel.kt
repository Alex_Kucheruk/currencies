package com.currencies.core

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch

fun <T> MutableSharedFlow<T>.emitWithScope(scope: CoroutineScope, value: T) {
    scope.launch { emit(value) }
}