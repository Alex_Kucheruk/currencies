package com.currencies.domain.repository

interface ConvertRepository {
    suspend fun convert(from: String, to: String, amount: Double): Double
}
