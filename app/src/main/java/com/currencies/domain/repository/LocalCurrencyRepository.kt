package com.currencies.domain.repository

interface LocalCurrencyRepository {
    val localCurrencyName: String
}
