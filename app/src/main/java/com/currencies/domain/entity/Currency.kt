package com.currencies.domain.entity

import androidx.compose.runtime.Stable

interface ICurrency {
    val currencyName: String
}

@Stable
data class Currency(
    override val currencyName: String
) : ICurrency

@Stable
data class AllCurrency(
    override val currencyName: String,
    val isAdded: Boolean
) : ICurrency
